# %% [markdown]
# # Electron Diffraction
# 

# %%
using Plots
using CSV, DataFrames
using LaTeXStrings, Latexify
import PhysicalConstants.CODATA2018: e as E, h as h, m_e as M
using Unitful
using LsqFit
import Statistics: std
pgfplotsx();
# pythonplot();

# %%
L = 0.135u"m"

# %% [markdown]
# ## Reading Data through file
# 
# In the `.csv` values are in `cm`
# 

# %%
# data = CSV.read("data.tsv", delim='\t', DataFrame)
data = CSV.read("data.csv", DataFrame)
# data = CSV.read("data.csv", DataFrame; type=Quantity)

# %%
D_1, D_2 = data[!, 1], data[!, 2];
D_1_units, D_2_units = D_1 .* 1u"cm", D_2 .* 1u"cm";

# %% [markdown]
# ### Calculating the `std` in diameter measurements
# 

# %%
relERR_D1 = std(D_1) ./ D_1
relERR_D2 = std(D_2) ./ D_2

# %%
U = [i for i in 3000:500:5000]
U_units = U .* 1u"V";

# %%
x = 1 ./ sqrt.(U);

# %% [markdown]
# ## Fitting the Curve with $y = k × x$
# 

# %%
@. st_line_origin(x, k) = k[1] * x;

# %%
fit_result_1 = curve_fit(st_line_origin, x, D_1, [159.0]);
k1 = fit_result_1.param;
k1_unit = k1[1] * 1u"cm*V^(1/2)"
fit_result_2 = curve_fit(st_line_origin, x, D_2, [159.0]);
k2 = fit_result_2.param;
k2_unit = k2[1] * 1u"cm*V^(1/2)"

k1_unit, k2_unit

# %% [markdown]
# ## Plotting the Graphs (with fit curves)
# 

# %%
x_plot = range(0, 0.02, 100);

# %%
# plot()
fig_width = 11.69
fig_height = 8.27
p = scatter(x, [D_1 D_2], label=[L"D_1" L"D_2"], ms=[4.5 3], shape=[:utriangle :rect], mc=:black)
plot!(x_plot, [st_line_origin(x_plot, k1) st_line_origin(x_plot, k2)], label=[L"fit curve for $D_1$" L"fit curve for $D_2$"], lw=1.5, lc=[:red :blue])
plot!(
    extra_kwargs=Dict(:plot => Dict("height" => "$(fig_height)in", "width" => "$(fig_width)in")),
    dpi=300,
    fontfamily="Computer Modern",
    legendfontsize=11,
    titlefontsize=20,
    tickfontsize=10,
    guidefontsize=15,
    ga=0.25,
    draw_arrow=true,
    formatter=:scientific,
    legend=:topleft
    # tex_output_standalone=true
)
xlabel!(L"\frac{1}{\sqrt{U}} ~ \left[V^{-1/2}\right]")
ylabel!(L"Inner diameter of rings $[cm]$")
title!("Diffraction of electrons in a polycrystalline graphite lattice")
# Plots.pdf("main")
display(p)

# %% [markdown]
# ## Calculating Lattice spacing
# 

# %%
d1_unit = (2 * L * h) / (k1_unit * sqrt(2 * M * E)) |> u"m"
d2_unit = (2 * L * h) / (k2_unit * sqrt(2 * M * E)) |> u"m"

println(round(convert(Float64, d1_unit / 1u"m"), sigdigits=5))
println(round(convert(Float64, d2_unit / 1u"m"), sigdigits=5))

# %%
d1_unit

# %%
d2_unit

# %%
relERR_k1 = standard_errors(fit_result_1)[1] * oneunit(k1_unit) / k1_unit
relERR_k2 = standard_errors(fit_result_2)[1] * oneunit(k2_unit) / k2_unit

relERR_k1, relERR_k2

# %%
err_d1 = relERR_k1 * d1_unit
err_d2 = relERR_k2 * d2_unit
err_d1, err_d2

# %% [markdown]
# $d_1 = (2.0707  ± 0.01964) × 10^{-10} ~\mathrm{m}$
# 
# $d_2 = (1.1973 ± 0.00571) × 10^{-10} ~\mathrm{m}$
# 

# %% [markdown]
# ## Calculate `de Broglie's wavelength`
# 

# %%
λ1 = (D_1_units .* d1_unit) / (2 * L) .|> u"pm";
λ2 = (D_2_units .* d2_unit) / (2 * L) .|> u"pm";

# %%
λ1

# %%
λ2

# %%
λ_th = (h) ./ (sqrt.(2 * M * E .* U_units)) .|> u"pm"

# %%
relERR_λ1 = sqrt.(relERR_D1 .^ 2 .+ relERR_k1^2);
relERR_λ2 = sqrt.(relERR_D2 .^ 2 .+ relERR_k2^2);

# %%
relERR_λ1

# %%
relERR_λ2

# %%
err_λ1 = λ1 .* relERR_λ1

# %%
err_λ2 = λ2 .* relERR_λ2


